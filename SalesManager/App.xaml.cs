﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SalesManager.View;
using SalesManager.View.Authentication;
using Xamarin.Essentials;

namespace SalesManager
{
    public partial class App : Application
    {
        public static string DatabaseLocation = string.Empty;
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new LoginView());
        } 
        
        public App(string databaselocation)
        {
            InitializeComponent();
            DatabaseLocation = databaselocation;
            var myValue = Preferences.Get("Username", null);
            if (myValue == null)
            {
                MainPage = new NavigationPage(new LoginviewMain());
            }
            else
            {
                MainPage = new NavigationPage(new MyTabbedPage());
            }
              
        }

        protected async override void OnStart()
        {
          
            await Permissions.RequestAsync<Permissions.Camera>();
            await Permissions.RequestAsync<Permissions.StorageRead>();
            await Permissions.RequestAsync<Permissions.StorageWrite>();
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
