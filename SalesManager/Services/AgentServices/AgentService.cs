﻿using Newtonsoft.Json;
using SalesManager.Constants;
using SalesManager.Services.RepoServices;
using SalesManagerChurchRequestObjects;
using SalesManagerWithdrawModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SalesManager.Services.AgentServices
{
    class AgentService : IAgentService
    {
        IGenericRepository genericRepository;
        public AgentService()
        {
            genericRepository = new Genericrepository();
        }

        public async Task<bool> Getagent<T>()
        {
            string uri = ApiConstants.Agent + Application.Current.Properties["userid"].ToString();

           var result = await genericRepository.GetAsync<T>(uri, Application.Current.Properties["Token"].ToString());

            if (result != null)
            {
                var agent = JsonConvert.SerializeObject(result);
                Application.Current.Properties["Agent"] = agent;
                await Application.Current.SavePropertiesAsync();
                return true;
            }
            return false;
        }   
        
        public async Task<bool> Getagentschurch<T>()
        {
            string uri = ApiConstants.GetagentLead + Application.Current.Properties["userid"].ToString();

           var result = await genericRepository.GetAsync<IEnumerable<T>>(uri, Application.Current.Properties["Token"].ToString());

            if (result != null)
            {
                var agentchurch = JsonConvert.SerializeObject(result);
                Application.Current.Properties["AgentsChurch"] = agentchurch;
                await Application.Current.SavePropertiesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> Getagentsprint<T>()
        {
            string uri = ApiConstants.Getagentssprint + Application.Current.Properties["userid"].ToString();

            var result = await genericRepository.GetAsync<IEnumerable<T>>(uri, Application.Current.Properties["Token"].ToString());

            if (result != null)
            {
                var agentchurch = JsonConvert.SerializeObject(result);
                Application.Current.Properties["AgentsSprint"] = agentchurch;
                await Application.Current.SavePropertiesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> GetBalance<T>()
        {
            string uri = ApiConstants.Balance + Application.Current.Properties["userid"].ToString();

            var result = await genericRepository.GetAsync<T>(uri, Application.Current.Properties["Token"].ToString());
            if (result != null)
            {
                Application.Current.Properties["CurrentBalance"] = JsonConvert.SerializeObject(result);
                await Application.Current.SavePropertiesAsync();
                return true;
            }
            return false;
        }

        public async Task<bool> PostChurch<T>(ChurchRequestObjects CRO)
        {
            var url = ApiConstants.CreateLead;
            var success = await genericRepository.PostAsync<T, ChurchRequestObjects>(url, CRO , Application.Current.Properties["Token"].ToString());
            if (success != null)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> WithdrawBalance<T>(WithdrawModel WDM)
        {
            var url = ApiConstants.withdraw;
            var success = await genericRepository.PostAsync<T, WithdrawModel>(url, WDM, Application.Current.Properties["Token"].ToString());
            if (success != null)
            {
                return true;
            }
            return false;
        }
    }
}
