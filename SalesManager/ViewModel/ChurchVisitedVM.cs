﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Rg.Plugins.Popup.Extensions;
using SalesManager.Data;
using SalesManager.Model;
using SalesManager.Services.AgentServices;
using SalesManager.Validation;
using SalesManager.ValidationRules;
using SalesManager.View;
using SalesManagerChurchRequestObjects;
using SalesManagerChurchResponseObject;
using SalesManagerModelChurch;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SalesManager.ViewModel
{
    class ChurchVisitedVM : BaseViewModel
    {
        INavigation navigation;

        IAgentService agentService;
        public ChurchVisitedVM(INavigation navigation)
        {
            IsNotBusy = true;
            this.navigation = navigation;
            churchname = new ValidatableObject<string>();
            address = new ValidatableObject<string>();
            churchphoneno = new ValidatableObject<string>();
            pastorphoneno = new ValidatableObject<string>();
            contactname = new ValidatableObject<string>();
            churchname = new ValidatableObject<string>();
            churchname = new ValidatableObject<string>();
            town = new ValidatableObject<string>();
            churchsize = new ValidatableObject<int>();

            agentService = new AgentService();
            AddValidations();
            MessagingCenter.Subscribe<ChurchVisitedVM>(this, "Done", (sender) => 
            {
              
            });
            MessagingCenter.Subscribe<ChurchVisitedVM,string>(this, "Pic", (sender,args) => 
            {
                Picture = args;
            });
        }

        public int id { get; set; }

        ValidatableObject<string> churchname; 
        public ValidatableObject<string> ChurchName { get => churchname; set => SetProperty(ref churchname,value) ; }

        ValidatableObject<string> address;
        public ValidatableObject<string> Address { get => address; set => SetProperty(ref address,value); }

        ValidatableObject<string> churchphoneno;
        public ValidatableObject<string> ChurchPhoneNumber { get => churchphoneno; set => SetProperty(ref churchphoneno, value); }

        ValidatableObject<string> pastorphoneno;
        public ValidatableObject<string> Pastorsphonenumber { get => pastorphoneno; set => SetProperty(ref pastorphoneno,value); }

        string picture;
        public string Picture { get => picture; set => SetProperty(ref picture,value); }

        ImageSource _source;
        public ImageSource source
        {
            get { return _source; }
            set { SetProperty(ref _source, value); }
        }

        private Stream _picturestream;
        public Stream picturestream
        {
            get { return _picturestream; }
            set { SetProperty(ref _picturestream, value); }
        }

        private string _picturepath;
        public string PicturePath
        {
            get { return _picturepath; }
            set { SetProperty(ref _picturepath, value); }
        }

        ValidatableObject<string> contactname;
        public ValidatableObject<string> ChurchContactName { get => contactname; set => SetProperty(ref contactname,value); }

        string website;
        public string Website { get => website ; set => SetProperty(ref website,value); }

        ValidatableObject<int> churchsize;
        public ValidatableObject<int> ChurchSize { get => churchsize; set => SetProperty(ref churchsize,value); }

        ValidatableObject<string> town;
        public ValidatableObject<string> Town { get => town; set => SetProperty(ref town,value); }

        double latitude;
        // public double Latitude { get ; set; }

        double longitude;
        //  public double Longitude { get; set; }
        
        double? altitude;
        //  public double Altitude { get; set; }

        public ICommand Viewpicture => new Command(async () => await navigation.PushPopupAsync(new Picturepopup(picture)));

        public ICommand Takepicture
        {
            get
            {
                return new Command(async () =>
                {

                    try
                    {

                        await CrossMedia.Current.Initialize();

                        if (!CrossMedia.Current.IsTakePhotoSupported)
                        {
                            await Application.Current.MainPage.DisplayAlert("", ":( You cant't take photo.", "OK");
                            return;
                        }
                        var status = await Permissions.RequestAsync<Permissions.StorageRead>();
                        await Permissions.RequestAsync<Permissions.StorageWrite>();
                        var status1 = await Permissions.RequestAsync<Permissions.Camera>();
                        var mediaOptions = new StoreCameraMediaOptions();
                        // if you want to take a picture use TakePhotoAsync instead of PickPhotoAsync
                        if (status == PermissionStatus.Granted)
                        {
                            var selectedImageFile = await CrossMedia.Current.TakePhotoAsync(mediaOptions);
                            // VideoPath = null;
                            Picture = selectedImageFile.Path;

                            source = ImageSource.FromStream(() =>
                            {
                                picturestream = selectedImageFile.GetStream();
                                selectedImageFile.Dispose();
                                return picturestream;
                            });
                            if (source == null)
                            {
                                await Application.Current.MainPage.DisplayAlert("Error", "Could not take the picture, please try again.", "Ok");
                                return;
                            }
                            else
                            {
                                await Application.Current.MainPage.DisplayAlert("Picture Selected", PicturePath, "OK");
                                MessagingCenter.Send(this, "Pic", picture);
                                //IsPicture = true;
                            }
                            //isbusy = false;
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                });

            }
        }

        public ICommand SubmitCommand => new Command(async() =>
        {
            if(picture != null)
            {
                var current = Connectivity.NetworkAccess;
                if (current == NetworkAccess.Internet)
                {
                    if (Validate())
                    {
                        IsNotBusy = false;
                        IsBusy = true;

                        await Getlocation();
                        if (!(latitude == default && longitude == default))
                        {
                            try
                            {
                                ChurchRequestObjects churchRequest = new ChurchRequestObjects()
                                {
                                    AgentId = Application.Current.Properties["userid"].ToString(),
                                    ChurchName = churchname.Value,
                                    Address = address.Value,
                                    ChurchPhoneNumber = churchphoneno.Value,
                                    Pastorsphonenumber = pastorphoneno.Value,
                                    Picture = picture,
                                    ChurchContactName = contactname.Value,
                                    Website = (website == null) ? default : website,
                                    ChurchSize = churchsize.Value,
                                    Latitude = latitude,
                                    Longitude = longitude,
                                    Altitude = (altitude == default) ? default : altitude,
                                    Town = town.Value,
                                    ContactDate = DateTime.Now,
                                };
                                var success = await agentService.PostChurch<ChurchResponseObject>(churchRequest);
                                if (success)
                                {
                                    Church church = new Church()
                                    {
                                        ChurchName = churchname.Value,
                                        Address = address.Value,
                                        ChurchPhoneNumber = churchphoneno.Value,
                                        Pastorsphonenumber = pastorphoneno.Value,
                                        Picture = picture,
                                        ChurchContactName = contactname.Value,
                                        Website = (website == null) ? default : website,
                                        ChurchSize = churchsize.Value,
                                        Latitude = latitude,
                                        Longitude = longitude,
                                        Altitude = (altitude == default) ? default : altitude,
                                        Town = town.Value,
                                        ContactDate = DateTime.Now,

                                    };
                                    int done = DataBase.Addchurch(church);
                                    if (done == 1)
                                    {
                                        await Application.Current.MainPage.DisplayAlert("Successful", "Church  has been added", "ok");
                                        Application.Current.MainPage = new NavigationPage(new MyTabbedPage());
                                    }
                                }
                                else
                                {
                                    await Application.Current.MainPage.DisplayAlert("Something went wrong", "Church was not saved", "ok");
                                }

                            }
                            catch
                            {
                                await Application.Current.MainPage.DisplayAlert("Something went wrong", "Church was not saved", "ok");
                                IsNotBusy = true;
                                IsBusy = false;
                            }

                           
                        }
                    }

                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("No Internet", "Failed to get location", "ok");
                }
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("No Image", "Please add the image of the church", "ok");
            }

        });

          public async Task Getlocation()
        {
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.High);
                var location = await Geolocation.GetLocationAsync(request);

                if (location != null)
                {
                    latitude = location.Latitude;
                    longitude = location.Longitude; 
                    altitude = location.Altitude;
                }
            }
            catch (FeatureNotSupportedException)
            {
                // Handle not supported on device exception
                await Application.Current.MainPage.DisplayAlert("Location", "Location not supported", "ok");
            }
            catch (FeatureNotEnabledException)
            {
                // Handle not enabled on device exception
                await Application.Current.MainPage.DisplayAlert("Location", "Please enable phone location", "ok");
            }
            catch (PermissionException)
            {
                // Handle permission exception
                await Application.Current.MainPage.DisplayAlert("Location", "Permission has not been given ", "ok");
            }
            catch (Exception)
            {
                // Unable to get location
                await Application.Current.MainPage.DisplayAlert("Location", "Unable to get locaction please try again", "ok");
            }
            finally
            {
                IsNotBusy = true;
                IsBusy = false;
            }
        }

        private void AddValidations()
        {
            churchname.Validations.Add(new IsNotNullOrEmptyRule<string>
            {
                ValidationMessage = "A churchname is required."
            });
            address.Validations.Add(new IsNotNullOrEmptyRule<string>
            {
                ValidationMessage = "An Address is required."
            });
            churchphoneno.Validations.Add(new Phonenumberrule<string>
            {
                ValidationMessage = "Phonenumber is incorrect."
            });
            pastorphoneno.Validations.Add(new Phonenumberrule<string>
            {
                ValidationMessage = "Phoneno is incorrect."
            });
            contactname.Validations.Add(new IsNotNullOrEmptyRule<string>
            {
                ValidationMessage = "A contactname is required."
            });
            town.Validations.Add(new IsNotNullOrEmptyRule<string>
            {
                ValidationMessage = "A town is required."
            });
        }

        private bool Validate()
        {
            bool isValidUser = ValidateChurchName() && Validateaddress() && ValidatePhoneNo() && Validatecontactname()&& ValidateTown() && PastorsPhoneNo();  
            return isValidUser;
        }
        private bool ValidateChurchName()
        {
            return churchname.Validate();
        } 
        private bool Validateaddress()
        {
            return address.Validate();
        }  
        private bool ValidatePhoneNo()
        {
            return churchphoneno.Validate();
        } 
        private bool PastorsPhoneNo()
        {
            return pastorphoneno.Validate();
        }
        private bool Validatecontactname()
        {
            return contactname.Validate();
        }  
        private bool ValidateTown()
        {
            return town.Validate();
        }

    }
}
