﻿using SalesManager.Services.AgentServices;
using SalesManagerWithdrawModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SalesManager.ViewModel.WithDrawVM
{
    class WithdrawViewModel  : BaseViewModel
    {
        IAgentService agentService;

        public WithdrawViewModel()
        {
            RegionInfo myRi1 = new RegionInfo("NG");
            agentService = new AgentService();
            Balance = myRi1.CurrencySymbol + string.Format("{0:N}", Preferences.Get("Balance", 0L));
            IsNotBusy = true;
        }

        string _balance;
        public string Balance
        {
            get => _balance;
            set => SetProperty(ref _balance, value);
        }

        long _withdrawamount;
        public long Withdrawamount
        {
            get => _withdrawamount;
            set => SetProperty(ref _withdrawamount, value);
        }



        public ICommand Withdraw => new Command(async() =>
        {
            IsNotBusy = false;
            IsBusy = true;
            if (Withdrawamount <= Preferences.Get("Balance", 0L))
            {
                if(Withdrawamount > 1000)
                {
                    WithdrawModel withdrawModel = new WithdrawModel()
                    {
                        Status = 0,
                        RequestDate = DateTime.Now.Date.ToString("MM/dd/yyyy"),
                        RequestTime = DateTime.Now.TimeOfDay.ToString(),
                        AgentId = Application.Current.Properties["userid"].ToString(),
                        Amount = Withdrawamount,
                    };
                    var success = await agentService.WithdrawBalance<WithdrawModel>(withdrawModel);
                    if (success)
                    {
                        await Application.Current.MainPage.DisplayAlert("", "Request is being processed", "Ok");
                        await Application.Current.MainPage.Navigation.PopToRootAsync();
                        IsNotBusy = true;
                        IsBusy = false;
                    }
                    else
                    {
                        IsNotBusy = true;
                        IsBusy = false;
                        await Application.Current.MainPage.DisplayAlert("", "You cannot withdraw more than you balance", "Ok");
                    }
                }
                else
                {
                    IsNotBusy = true;
                    IsBusy = false;
                    await Application.Current.MainPage.DisplayAlert("", "You cannot withdraw less than N1000", "Ok");
                }
              
            }
            else
            {
                IsNotBusy = true;
                IsBusy = false;
                await Application.Current.MainPage.DisplayAlert("", "You cannot withdraw more than you balance", "Ok");
            }
        });
    }
}
