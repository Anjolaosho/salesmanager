﻿using SalesManager.View;
using SalesManager.View.ViewHistory;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace SalesManager.ViewModel.HistoryVM
{
    class SummaryVM
    {
        INavigation navigation;
        public SummaryVM(INavigation navigation)
        {
            this.navigation = navigation;
        }
        public ICommand Today => new Command(() => navigation.PushAsync(new TodaysVisits(1))); 
        public ICommand Month => new Command(() => navigation.PushAsync(new MonthsVisits(2))); 
        public ICommand AllVisits => new Command(() => navigation.PushAsync(new AllVisits(3))); 
        public ICommand MonthSales => new Command(() => navigation.PushAsync(new OverallAllSales(4))); 
        public ICommand AllSales => new Command(() => navigation.PushAsync(new OverallAllSales(5))); 
    }
}
