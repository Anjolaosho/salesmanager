﻿using Newtonsoft.Json;
using Rg.Plugins.Popup.Extensions;
using SalesManager.Data;
using SalesManager.Exceptions;
using SalesManager.Model.APIResponseObjects;
using SalesManager.Services;
using SalesManager.Services.AuthServices;
using SalesManager.Services.RepoServices;
using SalesManager.Validation;
using SalesManager.ValidationRules;
using SalesManager.View;
using SalesManager.View.AllPopup;
using SalesManager.View.Authentication;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SalesManager.ViewModel.AuthVM
{
    class LoginVM : BaseViewModel
    {
        INavigation navigation;


        ILoginService loginService;
        public LoginVM(INavigation navigation, ILoginService loginService)
        {
            this.loginService = loginService;
            IsNotBusy = true;
            this.navigation = navigation;
            username = new ValidatableObject<string>();
            email = new ValidatableObject<string>();
            password = new ValidatableObject<string>();

            var myValue = Preferences.Get("Username", null);
            if (myValue == null)
            {
                AddValidations();
            }
        }

        ValidatableObject<string> username;
        public ValidatableObject<string> UserName { get => username; set => SetProperty(ref username,value); }


        ValidatableObject<string> email;
        public ValidatableObject<string> Email { get => email; set => SetProperty(ref email,value); }


        ValidatableObject<string> password;
        public ValidatableObject<string> Password { get => password; set => SetProperty(ref password, value); }

        

        bool _loginError = false;
        public bool LoginError
        {
            get
            {

                return _loginError;
            }
            set { SetProperty(ref _loginError, value); }
        }

        private void AddValidations()
        {
            username.Validations.Add(new IsNotNullOrEmptyRule<string>
            {
                ValidationMessage = "A username is required."
            });
        }

        private bool Validate()
        {
            bool isValidUser = ValidateUserName();
            return isValidUser;
        }
        private bool ValidateUserName()
        {
            return username.Validate();
        }


        public ICommand ValidateUserNameCommand => new Command(() => ValidateUserName());
        public ICommand Navtoforgotpassword => new Command(async() => await navigation.PushPopupAsync(new ForgotPasswordView()));

        public ICommand SubmitCommand => new Command(async() =>
        {
            try
            {
                LoginError = false;
                IsNotBusy = false;
                IsBusy = true;
                if (Validate())
                {
                    var success = await loginService.Login<LoginResponse>(username.Value, password.Value);
                    if (success)
                    {
                        var myValue = Preferences.Get("Username", null);
                        if (myValue == null)
                        {
                            Preferences.Set("Username", username.Value);
                        }
                        var loggeduser = JsonConvert.DeserializeObject<LoginResponse>(Application.Current.Properties["user"].ToString());
                        Application.Current.Properties["userid"] = loggeduser.CurrentId;
                        Application.Current.Properties["Token"] = loggeduser.AccessToken;
                        await Application.Current.SavePropertiesAsync();
                        Application.Current.MainPage = new NavigationPage(new MyTabbedPage());
                     
                    }

                }
                IsBusy = false;
                IsNotBusy = true;
                LoginError = true;
            }
            catch(HttpRequestExceptionEx ex)
            {
                await Application.Current.MainPage.DisplayAlert("Ops.. Something went wrong", $"Username or Password incorrect", "Ok");
                IsBusy = false;
                IsNotBusy = true;
                LoginError = true;
            }
            catch(Exception ex)
            {
                await Application.Current.MainPage.DisplayAlert("Ops.. Something went wrong", $"{ex.Message}", "Ok");
                IsBusy = false;
                IsNotBusy = true;
                LoginError = true;
            }
          
        });
    }
}
