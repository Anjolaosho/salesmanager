﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SalesManager.Validation
{
    public interface IValidity
    {
        bool IsValid { get; set; }
    }
}
