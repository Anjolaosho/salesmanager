﻿using SalesManager.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesManager.ValidationRules
{
    class Phonenumberrule<T> : IValidationRule<T>
    {
        //Error Message
        public string ValidationMessage { get; set; }

        // Implementation of The method that checks if an error has occured
        //Return false if there is an error
        //Return true if there is no error
        public bool Check(T value)
        {
            var str = value as string;
            if (string.IsNullOrEmpty(str))
            {
                return false;
            }
            else
            {
                return str.Length >= 10;
            }

        }
    }
}
