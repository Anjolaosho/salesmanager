﻿using Autofac;
using SalesManager.Services.AuthServices;
using SalesManager.Services.Navigationservices;
using SalesManager.ViewModel.AuthVM;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesManager.Dependencies
{
    public static class AllDependencies
    {
        public static IContainer container;

        public static void RegisterDependency()
        {
            var _container = new ContainerBuilder();

            _container.RegisterType<LoginVM>();


            _container.RegisterType<loginServices>().As<ILoginService>().SingleInstance();
            _container.RegisterType<NavigationService>().As<Xamarin.Forms.INavigation>().SingleInstance();



            container = _container.Build();
        }

        public static object Resolve(Type typeName)
        {
            return container.Resolve(typeName);
        }

        public static T Resolve<T>()
        {
            return container.Resolve<T>();
        }
    }
}
