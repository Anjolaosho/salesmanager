﻿using SalesManager.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SalesManager.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Picturepopup : Rg.Plugins.Popup.Pages.PopupPage
    {
        ChurchVisitedVM CVVM;
        public Picturepopup(string source)
        {
            InitializeComponent();
            image.Source = source;
            BindingContext = new ChurchVisitedVM(this.Navigation);
            MessagingCenter.Subscribe<ChurchVisitedVM, string>(this, "Pic", (sender, args) =>
            {
                image.Source = args;
            });
        }
    }
}