﻿using SalesManager.ViewModel.HistoryVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SalesManager.View.ViewHistory
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MonthsVisits : ContentPage
    {
        TodaysVisitsVM TVVM;
        public MonthsVisits(int position)
        {
            InitializeComponent();
            BindingContext = TVVM = new TodaysVisitsVM(position);
        }
    }
}