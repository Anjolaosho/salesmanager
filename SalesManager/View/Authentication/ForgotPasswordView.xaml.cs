﻿using SalesManager.ViewModel.AuthVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SalesManager.View.Authentication
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ForgotPasswordView : Rg.Plugins.Popup.Pages.PopupPage
    {
        public ForgotPasswordView()
        {
            InitializeComponent();
            BindingContext = new ForgotPasswordViewModel(this.Navigation);
        }
    }
}