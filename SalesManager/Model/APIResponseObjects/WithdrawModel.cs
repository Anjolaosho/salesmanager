﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace SalesManagerWithdrawModel
{
    public partial class WithdrawModel
    {
        [JsonProperty("withdrawerId")]
        public Guid WithdrawerId { get; set; }

        [JsonProperty("requestDate")]
        public string RequestDate { get; set; }

        [JsonProperty("requestTime")]
        public string RequestTime { get; set; }

        [JsonProperty("dateProcessed")]
        public DateTime DateProcessed { get; set; }

        [JsonProperty("timeProcessed")]
        public DateTime TimeProcessed { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("transactionId")]
        public Guid TransactionId { get; set; }

        [JsonProperty("agentName")]
        public string AgentName { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("processedBy")]
        public string ProcessedBy { get; set; }

        [JsonProperty("agentId")]
        public string AgentId { get; set; }

        [JsonProperty("transactionType")]
        public long TransactionType { get; set; }

        [JsonProperty("naration")]
        public string Naration { get; set; }

        [JsonProperty("transactionDate")]
        public DateTimeOffset TransactionDate { get; set; }
    }

    public partial class WithdrawModel
    {
        public static WithdrawModel FromJson(string json) => JsonConvert.DeserializeObject<WithdrawModel>(json, Converter.Settings);
    }

    public static class Serialize
    {
        public static string ToJson(this WithdrawModel self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
