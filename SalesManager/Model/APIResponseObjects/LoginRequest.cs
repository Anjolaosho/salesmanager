﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SalesManager.Model.APIResponseObjects
{
    class LoginRequest
    {
        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }
    }  
    
    class ForgotPasswordRequest
    {
        [JsonProperty("email")]
        public string Email { get; set; }
    }
}

